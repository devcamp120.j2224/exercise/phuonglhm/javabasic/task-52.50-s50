import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03.javabasic.s50.Order;
import com.devcamp.j03.javabasic.s50.Order2;
import com.devcamp.j03.javabasic.s50.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, j52.50_60!");
        ArrayList<Order> arrayList = new ArrayList<>();
        //Khởi tạo 4 object order với các tham số khác nhau
        Order Order0 = new Order();
        Order Order1 = new Order("Lan");
        Order Order2 = new Order(3, "Long", 80000);
        Order Order3 = new Order(4,"Nam",75000,new Date(), false, new String[] {"clay", "pencil", "eraser"});
        //thêm order object vào danh sách
        arrayList.add((Order0));
        arrayList.add((Order1));
        arrayList.add((Order2));
        arrayList.add((Order3));

        // in ra màn hình
        for (Order order: arrayList){
            System.out.println(order.toString());
        }

        ArrayList<Order2> array2 = new ArrayList<>();
        Person person1 = new Person("anan", 19,59);
        Person person2 = new Person();

        //Khởi tạo 4 object order2
        Order2 order21 = new Order2(5260,true,person1);
        // Order2 order22 = new Order2(11,"Devcamp120",120000);
        // Order2 order23 = new Order2(22,"Jerry",100000,false);
        Order2 order24 = new Order2(33,"Tom Cruise",200000,new Date(),true, new String[] {"Hawaii", "Bacon"}, person2);

         //thêm order object vào danh sách
         array2.add(order21);
        //  array2.add((order22));
        //  array2.add((order23));
         array2.add(order24);
 
         //in ra màn hình
         for (Order2 orders: array2){
             System.out.println(orders.toString());
         }
           // System.out.println(person24.getName());
        
    }
}
