package com.devcamp.j03.javabasic.s50;

import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


public class Order {
    int id; //id của order
    String customerName; //tên khách hàng
    long price; //tổng giá tiền 
    Date orderDate; //ngày thực hiện order
    boolean confirm; //đã xác nhận hay chưa
    String[] items; //danh sách mặt hàng đã mua

    //Khởi tạo 1 tham số customerName
    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"Book", "Pen", "Ruler"};
    }

    //Khởi tạo với tất cả tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"Book", "Pen", "Ruler"};
    }

    //Khởi tạo ko tham số
    public Order() {
        this("demo");
    }

    //Khởi tạo với 3 tham số
    
    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] {"Eggs", "Fish", "Oil", "Vegetables"});
    }

    @Override
    public String toString() {
        //định dạng tiêu chuẩn Việt Nam
        Locale.setDefault(new Locale("vi", "VN"));
        //định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        //Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //return trả ra chuỗi String
        return "Order [confirm = " + confirm + ", customerName = " + customerName + ", id = " + id 
                + ", items = " + Arrays.toString(items) 
                + ", orderDate = " + defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", price = " + usNumberFormat.format(price) + "]";
    }
}
