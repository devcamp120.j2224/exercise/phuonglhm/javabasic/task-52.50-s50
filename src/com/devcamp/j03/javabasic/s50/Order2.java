package com.devcamp.j03.javabasic.s50;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class Order2 {
    int id; //id của order
    String customerName; //tên khách hàng
    int price; //tổng giá tiền 
    Date orderDate = new Date(); //ngày thực hiện order
    Boolean confirm; //đã xác nhận hay chưa
    String[] items; //danh sách mặt hàng đã mua 
    Person buyer;// người mua, là một object thuộc class Person

    public Order2(int id, Boolean confirm, Person person) {
        this.id = id;
        this.confirm = confirm;
        this.buyer = person;
    }

    public Order2(int id, String customerName, Integer price, Person person) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.buyer = person;
    }
    
    public Order2(int id, String customerName, Integer price, Boolean confirm, Person person) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.confirm = confirm;
        this.buyer = person;
    }

    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = new Date();
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }


    @Override
    public String toString() {
        //định dạng tiêu chuẩn Việt Nam
        Locale.setDefault(new Locale("vi", "VN"));
        //định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss";
        DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        //Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //return trả ra chuỗi String
        return "Order [confirm = " + confirm + ", customerName = " + customerName + ", id = " + id 
        + ", items = " + Arrays.toString(items) +
        ", orderDate = " + defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
        + ", price = " + usNumberFormat.format(price) + ", buyer: " + buyer.getName()+ "]";

    }

}
